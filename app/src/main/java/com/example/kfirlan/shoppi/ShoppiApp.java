package com.example.kfirlan.shoppi;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.parse.Parse;
import com.parse.ParseObject;

/**
 * Created by kfirlan on 11/05/2015.
 */
public class ShoppiApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        Parse.enableLocalDatastore(this);

        ParseObject.registerSubclass(Product.class);
        ParseObject.registerSubclass(Discount.class);
        ParseObject.registerSubclass(Store.class);
        ParseObject.registerSubclass(History.class);

//        old version that omer uses
        Parse.initialize(this, "YYHS8nKNrbhp8rd0Hyt2TKYq3Qxhi79d4yGyRjBe", "gNcLsq44VOrR0wsJs40w5kTJU9WYVcotMJtxfqE1");
////        newer version that we use
//        Parse.initialize(this, "jOOA4Y7YyfmfFeabSEGsViTsuaNFHyzXvFAeYudd", "Hvg551nq7sExtklXIedMUzWDa5yB3ZFk4stjeeAl");
    }
}
