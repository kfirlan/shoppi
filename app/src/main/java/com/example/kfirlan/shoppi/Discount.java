package com.example.kfirlan.shoppi;

import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by kfirlan on 11/05/2015.
 */

@ParseClassName("Discount")
public class Discount extends ParseObject {
    public Discount(){}

    /**
     *
     * @param price of all products
     * @param description of the discount
     * @param store to add the discount into
     * @param amounts map of the products to add and there amount !!!!!!!assume the products are part of store!!!!!!!!
     */
    public Discount(double price, String description, final Store store,Map<Product,Integer> amounts,Date expiration) throws ParseException {
        setPrice(price);
        final List<ParseObject> discountsItems = new ArrayList<>();
        final ParseRelation<ParseObject> relation = getRelation("productsAmounts");
        put("description", description);
        put("expiration", expiration);
        boolean single = false;
        if (amounts.keySet().size()==1 && amounts.values().iterator().next()==1){
            single = true;
        }
        put("single",single);
        for (Product product : amounts.keySet()){
            ParseObject tmp = new ParseObject("DiscountItem");
            tmp.put("product", product);
            tmp.put("amount", amounts.get(product));
            tmp.put("expiration", expiration);
            tmp.put("single", single);
            product.addToDiscount(single,price);
            discountsItems.add(tmp);
        }
        final Discount discount=this;
        ParseObject.saveAll(discountsItems);
        for (ParseObject obj : discountsItems) {
            relation.add(obj);
        }
        save();
        store.addDiscount(discount);

    }

    private void setPrice(double price) {
        put("price",price);
    }

    public double getPrice() {
        return getDouble("price");
    }

    public boolean isSingle(){return getBoolean("single");}

    public String getDescription() {
        return getString("description");
    }

    public Map<Product,Integer> getProductsAmounts() throws ParseException {
        Map<Product,Integer> res = new HashMap<>();
        final ParseRelation<ParseObject> relation = getRelation("productsAmounts");
        List<ParseObject> discounts;
        discounts= relation.getQuery().include("product").find();
        for (ParseObject object : discounts){
            res.put((Product) object.getParseObject("product"),object.getInt("amount"));
        }
        return res;
    }

    public void destroyInDB(){
        ParseRelation<ParseObject> relation = getRelation("productsAmounts");
        final Discount tmp = this;
        relation.getQuery().findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e!=null)
                    throw new RuntimeException(e);
                try {
                    for (Product product: getProductsAmounts().keySet()){
                        product.removeFromDiscount(isSingle());
                    }
                    ParseObject.deleteAll(parseObjects);
                    tmp.delete();
                } catch (ParseException e1) {
                    throw new RuntimeException(e1);
                }
            }
        });
    }

    public static void deleteDiscountsForProduct(final Product product){
        ParseRelation<Discount> relation= product.getStore().getRelation("discounts");
        ParseQuery<Discount> query= relation.getQuery();
        query.findInBackground(new FindCallback<Discount>() {
            @Override
            public void done(List<Discount> discounts, ParseException e) {
                if (e!=null)
                    throw new RuntimeException(e);
                for (Discount discount : discounts){
                    try {
                        if (discount.getProductsAmounts().keySet().contains(product)) {
                            discount.destroyInDB();
                        }
                    }catch (ParseException e1){
                        throw new RuntimeException(e1);
                    }
                }
            }
        });
    }

    public static List<Discount> findDiscountsForProduct(final Product product) throws ParseException {
        ParseRelation<Discount> relation= product.getStore().getRelation("discounts");
        ParseQuery<Discount> query= relation.getQuery();
        List<Discount> discounts = new ArrayList<>();
        List<Discount> all = null;
        all = query.find();
        for (Discount discount : all){
            if (discount.getProductsAmounts().keySet().contains(product)){
                discounts.add(discount);
            }
        }
        return discounts;
    }

    public static void deleteAllOldDiscounts(Date lowerLimit){
        final ParseQuery<Discount> query = ParseQuery.getQuery(Discount.class);
        query.setLimit(1000);
        query.whereLessThan("expiration", lowerLimit);
        FindCallback<Discount> callback = new FindCallback<Discount>() {
            @Override
            public void done(List<Discount> discounts, ParseException e) {
                if (e!=null)
                    throw new RuntimeException(e);
                for(Discount discount : discounts)
                    discount.destroyInDB();
            }
        };
        query.findInBackground(callback);
    }

    public Date getExpirationDate(){
        return getDate("expiration");
    }

    public void setExpirationDate(Date expiration){
        put("expiration",expiration);
        saveInBackground();
    }
    public static class NegativePrice extends RuntimeException {
    }
}
