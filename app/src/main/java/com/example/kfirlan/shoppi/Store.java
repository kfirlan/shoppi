package com.example.kfirlan.shoppi;

import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by kfirlan on 11/05/2015.
 */
@ParseClassName("Store")
public class Store extends ParseObject {

    public Store(){}

    public Store(String storeName, ParseGeoPoint location,Category category,String address,ParseUser owner,String description) {
        put("name",storeName);
        put("location",location);
        put("category", category.ordinal());
        put("address", address);
        put("owner",owner);
        put("description",description);
        final ParseRelation<History> relation = getRelation("orders");
        final ParseRelation<Product> relation2 = getRelation("products");
        final ParseRelation<Discount> relation3=getRelation("discounts");
//        setACL(new ParseACL(owner));
//        getACL().setPublicReadAccess(true);
        saveInBackground();
    }

    public void addProduct(Product product){
        final ParseRelation<Product> relation= getRelation("products");
        relation.add(product);
        this.saveInBackground();
    }
    public void addProduct(List<Product> products){
        final ParseRelation<Product> relation= getRelation("products");
        for (Product product: products){
            relation.add(product);
        }
        saveInBackground();

    }


    public void addDiscount(Discount discount){
        final ParseRelation<Discount> relation=getRelation("discounts");
        relation.add(discount);
        saveInBackground();
    }

    public void addOrder(History order){
        final ParseRelation<History> relation= getRelation("orders");
        relation.add(order);
        saveInBackground();
    }
    public void addOrder(List<History> orders){
        final ParseRelation<History> relation= getRelation("orders");
        for (History order: orders){
            relation.add(order);
        }
        saveInBackground();
    }

    /**
     * delete a history
     * deleted history afterwards is useless
     * @param order
     */
    public void removeOrder(History order){
        final ParseRelation<History> relation= getRelation("orders");
        relation.remove(order);
        order.toAncient();
        saveInBackground();
    }

    /**
     * remove a product from a store and delete it afterwards
     * deleted product is useless
     * @param product
     */
    public void removeProduct(Product product){
        final ParseRelation<Product> relation= getRelation("products");
        relation.remove(product);
        product.invalidate();
        saveInBackground();
    }

    public void removeDiscount(Discount discount){
        final ParseRelation<Discount> relation=getRelation("discounts");
        relation.remove(discount);
        discount.destroyInDB();
        saveInBackground();
    }

    public ParseGeoPoint getLocation() {
        return getParseGeoPoint("location");
    }

    public String getStoreName() {
        return getString("name");
    }

    public List<Product> getProducts() throws ParseException {
        ParseRelation<Product> tmp= getRelation("products");
        return tmp.getQuery().find();
    }

    public List<Discount> getDiscounts() throws ParseException {
        ParseRelation<Discount> tmp= getRelation("discounts");
         return tmp.getQuery().find();

    }

    public List<History> getOrders() throws ParseException {
        ParseRelation<History> tmp= getRelation("orders");
        return tmp.getQuery().orderByAscending("createdAt").include("user").find();
    }
    public Category getCategory(){
        return Category.values()[getInt("category")];
    }
    public String getAddress(){return getString("address");}

    public void setLocation(double latitude,double longitude){
        put("location",new ParseGeoPoint(latitude,longitude));
        saveInBackground();
    }

    public void setLocation(ParseGeoPoint location){
        put("location",location);
        saveInBackground();
    }

    public void setAddress(String address){
        put("address",address);
        saveInBackground();
    }

    public void setCategory(Category category){
        put("category",category.ordinal());
        saveInBackground();
    }

    public String getDescription(){return getString("description");}

    public void setDescription(String description){
        put("description",description);
        saveInBackground();
    }

    public void setName(String name){
        put("name",name);
        saveInBackground();
    }

    public ParseUser getOwner(){
        if (containsKey("owner"))
            return getParseUser("owner");
        return null;
    }


}
