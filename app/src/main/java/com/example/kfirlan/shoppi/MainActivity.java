package com.example.kfirlan.shoppi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void addToParse(View view) throws ParseException {
        Toast.makeText(getApplicationContext(), "unimplemented", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_parse_stuff, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void checkGetParseLocation(View view) throws ParseException, InterruptedException {

//        Product orange = SearchUtils.INSTANCE.cheapestProduct("ban",200,0).get(0);
//        Store store = orange.getStore();
//        Map<Product,Integer> map = new HashMap<>();
//        map.put(orange,1);
//        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.DAY_OF_YEAR,-2);
//        new Discount(50,"die discount",store,map,calendar.getTime());
        final Context context = this;
       ParseQuery<Product> query = ParseQuery.getQuery(Product.class);
        query.setLimit(1000);
        query.whereDoesNotExist("storeLocation");
        query.include("store");
        query.findInBackground(new FindCallback<Product>() {
            @Override
            public void done(List<Product> products, ParseException e) {
                for (Product product : products){
                    product.put("storeLocation",product.getStoreLocation());
                    product.saveInBackground();
                }
                Toast.makeText(context,"number of affected products is:"+products.size(),Toast.LENGTH_SHORT).show();
            }
        });

    }


}
