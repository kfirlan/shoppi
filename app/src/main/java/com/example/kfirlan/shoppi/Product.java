package com.example.kfirlan.shoppi;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.widget.ImageView;

import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Created by kfirlan on 11/05/2015.

 */
@ParseClassName("Product")
public class Product extends ParseObject {
    public Product (){}

    /**
     * save product on create (in the background)
     * @param name
     * @param price
     * @param store
     * @param description
     * @param category
     * @param picture
     */
    public static Product buildProduct (String name, double price, final Store store, String description,Category category, byte[] picture) throws ParseException {
        final Product res = new Product();
        res.put("name", name);
        res.put("price", price);
        res.put("store",store);
        res.put("description", description);
        res.put("category", category.ordinal());
        res.put("valid",true);
        res.put("discounts",0);
        res.put("storeLocation",store.getLocation());
        res.put("origPrice",price);
        final ParseFile file = new ParseFile(name.replaceAll("[^\\w]","")+"_"+store.getStoreName().replaceAll("[^\\w]","")+".jpg",picture);
        file.save();
        res.put("picture", file);
        res.save();
        store.addProduct(res);

//        file.saveInBackground(new SaveCallback() {
//            @Override
//            public void done(ParseException e) {
//                res.put("picture", file);
//                try {
//                    res.save();
//                } catch (ParseException e1) {
//                    throw new RuntimeException(e1);
//                }
//                store.addProduct(res);
//            }
//        });
        return res;
    }

    public void setPrice(final double price) {
        put("origPrice", price);
        if (price < getPrice() || !isOnDiscount()){
            put("price",price);
        }
        final Product product = this;
        saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e!=null)
                    throw new RuntimeException(e);
                List<Discount> discounts = null;
                try {
                    discounts = Discount.findDiscountsForProduct(product);
                } catch (ParseException e1) {
                    throw new RuntimeException(e1);
                }
                for (Discount discount : discounts){
                    if (discount.getPrice() >= price && discount.isSingle()){
                        discount.destroyInDB();
                    }
                }
            }
        });
    }

    /**
     * must save product and file before use
     * prodcuts brought by searchUtils are already saved
     * @return
     */
    public String getPicture(){
        return getPictureAux().getUrl();
    }
    private ParseFile getPictureAux(){
        return getParseFile("picture");
    }

    public String getName() {
        return getString("name");
    }

    public double getPrice() {
        return getDouble("price");
    }

    public double getOrigPrice(){return getDouble("origPrice");}

    public String getStoreName() {
        return getStore().getStoreName();
    }

    public ParseGeoPoint getStoreLocation() {return getParseGeoPoint("storeLocation");}

    public String getDescription() {
        return getString("description");
    }

    public Category getCategory(){return Category.values()[getInt("category")];}

    public void setName(String name){put("name",name);}

    public void setDescription(String description){put("description",description);}

    public void setPicture(final byte[] picture){
        final ParseFile tmp = new ParseFile("profile.jpg",picture);
        tmp.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e!=null)
                    throw new RuntimeException(e);
                put("picture",tmp);
                saveInBackground();
            }
        });
    }

    public void setCategory(Category category){put("category",category.ordinal());}
    public static class DistanceComparator implements Comparator<Product>{
        ParseGeoPoint center;
        public DistanceComparator(ParseGeoPoint center){
            this.center=center;
        }

        @Override
        public int compare(Product product1, Product product2) {
            double r1=center.distanceInKilometersTo(product1.getStoreLocation());
            double r2=center.distanceInKilometersTo(product2.getStoreLocation());
            if (r1 == r2)
                return 0;
            if (r1 > r2)
                return 1;
            return -1;
        }
    }

    public void addToDiscount(boolean single,double price){
        increment("discounts",1);
        if (single){
            put("price",price);
        }
        saveInBackground();
    }

    public void removeFromDiscount(boolean single){
        increment("discounts",-1);
        if (single){
            put("price",getOrigPrice());
        }
        saveInBackground();
    }

    public boolean isOnDiscount(){
        return getInt("discounts")>0;
    }

    public void invalidate(){
        put("valid",false);
        saveInBackground();
        Discount.deleteDiscountsForProduct(this);
    }

    public void validate(){
        put("valid",true);
        saveInBackground();
    }

    public Store getStore(){
        return ((Store) getParseObject("store"));
    }

}
