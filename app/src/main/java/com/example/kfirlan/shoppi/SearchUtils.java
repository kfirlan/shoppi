package com.example.kfirlan.shoppi;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by kfirlan on 19/05/2015.
 */
public enum SearchUtils {
    INSTANCE;
    private ParseQuery<Product> cheapestProductAux(String product_name,double max_price,double min_price){
        ParseQuery<Product> query = ParseQuery.getQuery(Product.class);
        query.whereContains("name",product_name);
        query.whereGreaterThanOrEqualTo("price", min_price);
        query.whereLessThanOrEqualTo("price", max_price);
        query.orderByAscending("price");
        query.include("store");
        query.whereEqualTo("valid",true);
        return query;
    }
    public List<Product> cheapestProduct(String product_name,double max_price,double min_price) throws ParseException {
        List<Product> res;
        res= cheapestProductAux(product_name,max_price,min_price).find();
        return res;
    }

    /**
     * a blocking call that get from the database
     * list of the products that contain product_name in their name
     * the list is ordered by price, where the price range is as specified
     * @param product_name
     * @param max_price
     * @param min_price
     * @param category
     * @return list of products that match the criteria
     */
    public List<Product> cheapestProduct(String product_name,double max_price,double min_price,Category category) throws ParseException {
        ParseQuery<Product> query=cheapestProductAux(product_name,max_price,min_price);
        query.whereEqualTo("category",category.ordinal());
        return query.find();
    }
    // if radius is negative then there is no max distance
    private ParseQuery<Product> closestProductAux(String product_name,ParseGeoPoint user_location,double radius){
        ParseQuery<Product> query = ParseQuery.getQuery(Product.class);
        query.whereContains("name", product_name);
        query.include("store");
        query.whereEqualTo("valid",true);
        if (radius <= 0)
            query.whereNear("storeLocation",user_location);
        else
            query.whereWithinKilometers("storeLocation",user_location,radius);
        query.orderByAscending("price");
        return query;
    }



    /**
     * a blocking call to get closest products in range  that contain product_name in their name from the database.
     * the products are ordered first by distance secondly by price
     * @param product_name
     * @param user_location
     * @param max_range
     * @return list of products
     */
    public List<Product> closestProduct(String product_name,ParseGeoPoint user_location,double max_range) throws ParseException {
        List<Product> res ;
        res = closestProductAux(product_name,user_location,max_range).find();
        Collections.sort(res,new Product.DistanceComparator(user_location));
        return res;
    }

    public List<Product> closestProduct(String product_name,ParseGeoPoint user_location,
                                        double max_range, double max_price, double min_price) throws ParseException {
        List<Product> res ;
        res = closestProductAux(product_name,user_location,max_range)
                    .whereGreaterThanOrEqualTo("price", min_price)
                    .whereLessThanOrEqualTo("price", max_price)
                    .find();
        Collections.sort(res,new Product.DistanceComparator(user_location));
        return res;
    }

    /**
     * a blocking call to get closest products in range  that contain product_name in their name from the database.
     * the products are ordered first by distance secondly by price
     * equal to closestProduct(product_name,user_location,-1)
     * @param product_name
     * @param user_location
     * @return list of products
     */
    public List<Product> closestProduct(String product_name,ParseGeoPoint user_location) throws ParseException {
        return closestProduct(product_name,user_location,-1);
    }

    public List<Product> closestProduct(String product_name,ParseGeoPoint user_location, double max_price, double min_price) throws ParseException {
        return closestProduct(product_name,user_location,-1, max_price, min_price);
    }

    /**
     * a blocking call to get closest products from the database that fit the category and
     * contain product_name in their name
     * the products are ordered first by distance secondly by price
     * equal to closestProduct(product_name,user_location,-1,category);
     * @param product_name
     * @param user_location
     * @param category
     * @return list of products
     */
    public List<Product> closestProduct(String product_name,final ParseGeoPoint user_location,Category category) throws ParseException {
        return closestProduct(product_name,user_location,-1,category);
    }

    public List<Product> closestProduct(String product_name,final ParseGeoPoint user_location,Category category,
             double max_price, double min_price) throws ParseException {
        return closestProduct(product_name,user_location,-1,category, max_price, min_price);
    }

    /**
     * a blocking call to get closest products from the database that fit the category and
     * contain product_name in their name and are within max_range in kilometers
     * the products are ordered first by distance secondly by price
     * @param product_name
     * @param user_location
     * @param max_range
     * @param category
     * @return list of products
     */
    public List<Product> closestProduct(String product_name,final ParseGeoPoint user_location,double max_range,Category category,
                                        double max_price, double min_price) throws ParseException {
        ParseQuery<Product> query=closestProductAux(product_name,user_location, max_range);
        query.whereEqualTo("category",category.ordinal());
        List<Product> res ;
            res = query.whereGreaterThanOrEqualTo("price", min_price)
                    .whereLessThanOrEqualTo("price", max_price)
                    .find();

        Collections.sort(res,new Product.DistanceComparator(user_location));
        return res;
    }

    public List<Product> closestProduct(String product_name,final ParseGeoPoint user_location,double max_range,Category category) throws ParseException {
        ParseQuery<Product> query=closestProductAux(product_name,user_location, max_range);
        query.whereEqualTo("category",category.ordinal());
        List<Product> res ;
        res = query.find();
        Collections.sort(res,new Product.DistanceComparator(user_location));
        return res;
    }

    /**
     * return the store with the exact name as store_name
     * a blocking call
     * faster then getStoresMatch
     * @param store_name
     * @return
     */
    public Store getStore(String store_name) throws ParseException {
        ParseQuery<Store> query=ParseQuery.getQuery(Store.class);
        query.whereEqualTo("name",store_name);
        return query.getFirst();
    }

    /**
     * return all the stores that match the specified
     * @param store_name
     * @return
     */
    public List<Store> getStoresMatch(String store_name) throws ParseException {
        ParseQuery<Store> query=ParseQuery.getQuery(Store.class);
        query.whereContains("name", store_name);
            return query.find();
    }
    /**
     * return all the stores that match the specified
     * @param store_name
     * @param category
     * @return
     */
    public List<Store> getStoreMatch(String store_name,Category category) throws ParseException {
        ParseQuery<Store> query=ParseQuery.getQuery(Store.class);
        query.whereContains("name", store_name);
        query.whereEqualTo("category",category.ordinal());
        return query.find();
    }

    /**
     * sort the searchResult by the stores of the products in cart,
     * the higher the number of products from the same store the closest
     * it will get to the start of the list.
     * @param searchResult from a query
     * @param cart the products in the cart
     */
    public void sortByCart (List<Product> searchResult,final List<Product> cart){
        final List<String> stores= new ArrayList<>();
        for (Product product:cart){
            stores.add(product.getStore().getObjectId());
        }
        Comparator<Product> tmp=new Comparator<Product>() {
            @Override
            public int compare(Product product, Product product2) {
                Integer n1=Collections.frequency(stores,product.getStore().getObjectId());
                Integer n2=Collections.frequency(stores,product2.getStore().getObjectId());
                return n2.compareTo(n1);
            }
        };
        Collections.sort(searchResult,tmp);
    }

    /**
     * return the users store or null if he dont hav one
     * @param user
     * @return
     */
    public Store findMyStore(ParseUser user) throws ParseException {
        ParseQuery<Store> query=ParseQuery.getQuery(Store.class);
        query.whereEqualTo("owner",user);
        try {
            return query.getFirst();
        } catch (ParseException e) {
            if(ParseException.OBJECT_NOT_FOUND == e.getCode()){
                return null;
            }
            throw (e);
        }
    }

    public boolean internetConnectionError(Throwable e){
        if (e.getClass() == ParseException.class){
            ParseException error = (ParseException) e;
            return error.getCode() == ParseException.CONNECTION_FAILED;
        }
        Throwable cause=e.getCause();
        if (cause!=null && cause.getClass() == ParseException.class){
            ParseException error = (ParseException) cause;
            return error.getCode()==ParseException.CONNECTION_FAILED;
        }
        return false;
    }
    public static class ParseQueryFailed extends RuntimeException{}
}
