package com.example.kfirlan.shoppi;

import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by kfirlan on 26/05/2015.
 */
@ParseClassName("History")
public class History extends ParseObject{
    public History(){}

    /**
     *
     * @param user
     * @param bought the shopping cart
     * @param prices the prices of a cart by {storeId , price}
     */
    public static void saveHistoryToDataBase (final ParseUser user,List<Product> bought,final Map<String,Double> prices){
        final List<ParseObject> historyItems = new ArrayList<>();
        final Map<String,List<ParseObject>> storesHistories = new HashMap<>();
        List<String> alreadyIn = new ArrayList<>();
        for (final Iterator iter = bought.iterator(); iter.hasNext(); ) {
            Product product = (Product)iter.next();
            if (alreadyIn.contains(product.getObjectId())){
                continue;
            }
            ParseObject tmp = new ParseObject("HistoryItem");
            tmp.put("product", product);
            tmp.put("amount", Collections.frequency(bought, product));
            Store products_store = product.getStore();
            alreadyIn.add(product.getObjectId());
            historyItems.add(tmp);
            List<ParseObject> storeHistoryItems = storesHistories.get(products_store.getObjectId());
            if (storeHistoryItems == null){
                storeHistoryItems = new ArrayList<>();
                storesHistories.put(products_store.getObjectId(),storeHistoryItems);
            }
            storeHistoryItems.add(tmp);
        }
        ParseObject.saveAllInBackground(historyItems,new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e!=null)
                    throw new RuntimeException(e);
                for (final String storeId : storesHistories.keySet()){
                    History history = new History();
                    history.put("user", user);
                    history.put("ancient",false);
                    history.put("ready",false);
                    final ParseRelation<ParseObject> history_relation = history.getRelation("bought");
                    Store tmp = null;
                    try {
                        tmp = ParseQuery.getQuery(Store.class).get(storeId);
                    } catch (ParseException e1) {
                        throw new RuntimeException(e1);
                    }
                    history.put("store",tmp);
                    double price = prices.get(storeId);
                    history.put("price",price);
                    for (ParseObject obj :storesHistories.get(storeId)){
                        history_relation.add(obj);
                    }
                    try {
                        history.save();
                    } catch (ParseException e1) {
                        throw new RuntimeException(e1);
                    }
                    tmp.addOrder(history);
                    tmp.saveInBackground();
                }
            }
        });
    }

    public static Map<String,Double> pricesWithoutDiscounts(List<Product> products){
        Map<String,Double> res = new HashMap<>();
        for (Product product : products){
            Store store = product.getStore();
            Double x = res.get(store.getObjectId());
            if (x == null){
                res.put(store.getObjectId(),product.getPrice());
            }else {
                res.put(store.getObjectId(),product.getPrice()+x);
            }
        }
        return res;
    }

    public ParseUser getUser(){
        return getParseUser("user");
    }

    public List<Product> getProducts() throws ParseException {
        ParseRelation<ParseObject> tmp= getRelation("bought");
        List<ParseObject> objects;
        objects = tmp.getQuery().include("product").find();
        List<Product> products = new ArrayList<>();
        for (ParseObject obj : objects){
            Product product = (Product) obj.get("product");
            int amount = obj.getInt("amount");
            for (int i=0; i < amount ; ++i){
                products.add(product);
            }
        }
        return products;
    }

    public void toAncient(){
        put("ancient",true);
        saveInBackground();
    }

    public boolean isAncient(){
        return getBoolean("ancient");
    }

    public void setReady(boolean value){
        put("ready",value);
        saveInBackground();
    }

    public boolean isReady(){
        return getBoolean("ready");
    }



    /**
     * get all histories of the user sorted by date from recent to latest
     * @param user
     * @return
     */
    public static List<History> getHistoryForUser(ParseUser user) throws ParseException {
        ParseQuery<History> query = ParseQuery.getQuery(History.class);
        query.whereEqualTo("user", user);
        query.orderByDescending("createdAt");
        query.include("store");
        List<History> tmp;
        tmp=query.find();
        return tmp;

    }

    public Store getStore(){
         Store res=(Store) getParseObject("store");
        return res;
    }

    public Date getDate(){
        return getCreatedAt();
    }

    public double getPrice(){
        return getDouble("price");
    }
}
