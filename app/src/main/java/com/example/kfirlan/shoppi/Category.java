package com.example.kfirlan.shoppi;

/**
 * Created by kfirlan on 14/05/2015.
 */
public enum Category {
    FOOD ,
    ELECTRONICS,
    TOOLS,
    FURNITURE
}
